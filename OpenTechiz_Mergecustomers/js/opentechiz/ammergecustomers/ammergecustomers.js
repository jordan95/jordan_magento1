AmMainCustomerDropdown = Class.create();

AmMainCustomerDropdown.prototype = {
    allHeader : null,
    nameIndex : 2,
    emailIndex : 3,
    selectedCustomers : [],
    dropdownSelector: '[name="main_customer"]',

    initialize : function() {
        this.allHeader = $$('.headings').first().select('th');
        this.createDropdownListener();
    },

    createDropdownListener : function () {
        this.setIndexes();
        this.setListenerOnCheckbox();
        this.setAjaxListener();
    },

    setIndexes : function () {
        this.allHeader.forEach(function(header) {
            if (header.querySelector('[name="name"]')) {
                this.nameIndex = header.cellIndex;
            } else if (header.querySelector('[name="email"]')) {
                this.emailIndex = header.cellIndex;
            }
        });
    },

    setListenerOnCheckbox : function () {
        var self = this;
        $$('body').first().on('click', '.a-center ', function(event, element) {
            var customerRows = element.parentElement.select('td');
            var label = customerRows[self.nameIndex].textContent.trim() + ' ' + customerRows[self.emailIndex].textContent.trim();
            var value = element.select('.massaction-checkbox').first().value;
            var checkboxIsChecked =element.select('.massaction-checkbox').first().checked;

            $$(self.dropdownSelector).forEach(function (dropdown) {
                if(checkboxIsChecked) {
                    self.selectedCustomers[value] = label;
                    dropdown.insert(new Element('option', {value: value}).update(label));
                } else {
                    delete self.selectedCustomers[value];
                    dropdown.remove(dropdown.select('[value="' + value + '"]').first().index);
                }
            });
        });
    },

    setAjaxListener : function () {
        var self = this;
        Ajax.Responders.register({
            onComplete: function() {
                $$(self.dropdownSelector).forEach(function (dropdown) {
                    self.selectedCustomers.forEach(function (customer, key) {
                        dropdown.insert(new Element('option', {value: key}).update(customer));
                    });
                });
            }
        });
    }
};

document.observe("dom:loaded", function(){
    AmMainCustomerDropdownObj = new AmMainCustomerDropdown();
});
