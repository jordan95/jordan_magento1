<?php
/**
 * @author Jordan Tran
 * @package OpenTechiz_Mergecustomers
 */



class OpenTechiz_Mergecustomers_Model_Observer
{
    /**
     * @param $observer
     */
    public function onPrepareMassactionBefore($observer)
    {
        $block = $observer->getEvent()->getBlock();
        
        if ($block instanceof Mage_Adminhtml_Block_Customer_Grid
            && Mage::getSingleton('admin/session')->isAllowed('customer/manage/mergecustomers')
        ) {
            $this->setMergeCustomersMenuInGrid($block);
        }
    }

    /**
     * @param $block
     */
    protected function setMergeCustomersMenuInGrid($block)
    {
        $block->getMassactionBlock()->addItem(
            'merge_customers',
            array(
                'label' => Mage::helper('ammergecustomers')->__('Merge Customers'),
                'url' => $block->getUrl('*/*/mergeCustomers'),
                'confirm' => Mage::helper('ammergecustomers')->__(
                    'Attention: merged customers will be deactivated. Are you sure?'
                ),
                'additional'   => array(
                    'visibility'    => array(
                        'name'     => 'main_customer',
                        'type'     => 'select',
                        'class'    => 'required-entry',
                        'label'    => Mage::helper('ammergecustomers')->__('Choose Main Account'),
                        'values'   => '',
                        'customerId' => ''
                    )
                )
            )
        );
    }
}
