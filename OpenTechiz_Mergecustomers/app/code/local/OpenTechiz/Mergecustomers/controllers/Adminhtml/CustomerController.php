<?php
/**
 * @author Jordan Tran
 * @package OpenTechiz_Mergecustomers
 */


class OpenTechiz_Mergecustomers_Adminhtml_CustomerController extends Mage_Adminhtml_Controller_Action
{
    public function mergeCustomersAction()
    {
        $customersIds = $this->getRequest()->getParam('customer');


        if (count($customersIds) < 2) {
            Mage::getSingleton('adminhtml/session')
                ->addError(Mage::helper('ammergecustomers')->__('Please select minimum 2 customers.'));
        } else {
            try {
                $mainCustomerId = (int)$this->getRequest()->getParam('main_customer');
                $mainCustomer = Mage::getModel('customer/customer')->load($mainCustomerId);

                foreach ($customersIds as $customerId) {
                    if ($customerId == $mainCustomerId) {
                        continue;
                    }

                    $this->mergeSalesOrderInfo($customerId, $mainCustomer)
                        ->mergeAddresses($customerId, $mainCustomer);
                                   
                }
            } catch (Exception $e) {
                Mage::log($e->getMessage(), null, 'ammergecustomers.log', true);
                Mage::getSingleton('adminhtml/session')
                    ->addError(Mage::helper('ammergecustomers')->__('Error occurred, view log file.'));
            }

            Mage::getSingleton('adminhtml/session')
                ->addSuccess(
                    count($customersIds) - 1 . Mage::helper('ammergecustomers')->__(' customers have been merged.')
                );
        }

        $this->_redirect('*/*/index');
    }

    /**
     * @param $customerId
     * @param $mainCustomer
     * @return $this
     */
    protected function mergeSalesOrderInfo($customerId, $mainCustomer)
    {
        $ordersCustomer = Mage::getModel('sales/order')
            ->getCollection()
            ->addFieldToFilter('customer_id', $customerId);

        foreach ($ordersCustomer as $order) {
            $order->addData(array(
                'customer_id' => $mainCustomer->getId(),
                'customer_firstname' => $mainCustomer->getFirstname(),
                'customer_lastname' => $mainCustomer->getLastname(),
                'customer_email' => $mainCustomer->getEmail()
            ));
        }

        $ordersCustomer->save();

        return $this;
    }

    /**
     * @param $customerId
     * @param $mainCustomer
     * @return $this
     */
    protected function mergeAddresses($customerId, $mainCustomer)
    {
        $addresses = Mage::getModel('customer/address')
            ->getCollection()
            ->addFieldToFilter('parent_id', $customerId);

        foreach ($addresses as $address) {
            $address->setParentId($mainCustomer->getId());
        }

        $addresses->save();

        return $this;
    }



    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('customer/manage/mergecustomers');
    }
}
